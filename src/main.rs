extern crate sdl2;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::pixels::Color;
use sdl2::rect::Rect;
extern crate clap;
use clap::{App, Arg};

use std::io::Read;
use std::fs::File;
use std::thread::{sleep, spawn};
use std::time::{Duration};
use std::sync::{RwLock, Arc};
use std::ops::{Deref};

mod cpu;
use cpu::{CPU, DebugFlags, Config, GFX};

const WIDTH: u32 = 640;
const HEIGHT: u32 = 320;

const PROG_NAME: &str = "Chip8 emulator";

#[derive(Debug, Clone, Copy)]
enum SupportedKeyborads {
    QWERTY,
    AZERTY,
}

impl SupportedKeyborads {
    fn set_key(self, keys: &mut [bool; 16], value: bool, kc: Keycode) -> () {
        use self::SupportedKeyborads::*;
        use Keycode::*;
        match self {
            AZERTY => {
                match kc {
                    Num1 => keys[1] = value,
                    Num2 => keys[2] = value,
                    Num3 => keys[3] = value,
                    Num4 => keys[0xc] = value,
                    A    => keys[4] = value,
                    Z    => keys[5] = value,
                    E    => keys[6] = value,
                    R    => keys[0xd] = value,
                    Q    => keys[7] = value,
                    S    => keys[8] = value,
                    D    => keys[9] = value,
                    F    => keys[0xe] = value,
                    W    => keys[0xa] = value,
                    X    => keys[0] = value,
                    C    => keys[0xb] = value,
                    V    => keys[0xc] = value,
                    _    => (),
                }
            },
            QWERTY => {
                match kc {
                    Num1 => keys[1] = value,
                    Num2 => keys[2] = value,
                    Num3 => keys[3] = value,
                    Num4 => keys[0xc] = value,
                    Q    => keys[4] = value,
                    W    => keys[5] = value,
                    E    => keys[6] = value,
                    R    => keys[0xd] = value,
                    A    => keys[7] = value,
                    S    => keys[8] = value,
                    D    => keys[9] = value,
                    F    => keys[0xe] = value,
                    Z    => keys[0xa] = value,
                    X    => keys[0] = value,
                    C    => keys[0xb] = value,
                    V    => keys[0xc] = value,
                    _    => (),
                }
            },
        }
    }
}

fn gui(kbd: SupportedKeyborads,
       stop: &RwLock<bool>,
       array_receiver: &RwLock<GFX>,
       input_sender: &RwLock<[bool; 16]>) -> () {

    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();

    let window = video_subsystem.window(PROG_NAME, WIDTH, HEIGHT)
        .position_centered()
        .opengl()
        .build()
        .unwrap();

    let mut canvas = window
        .into_canvas()
        //.present_vsync()
        .build()
        .unwrap();

    canvas.set_draw_color(Color::RGB(0,0,0));
    canvas.clear();
    canvas.present();

    let mut event_pump = sdl_context.event_pump().unwrap();

    let mut running = true;
    let mut keys = [false; 16];

    while running {
        {
            let stop_lock = stop.read().unwrap();
            if *stop_lock {
                break;
            }
        }

        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. } => {
                    running = false;
                    break;
                },
                Event::KeyDown { keycode: Some(kc), .. } =>
                    kbd.set_key(&mut keys, true, kc),
                Event::KeyUp { keycode: Some(kc), .. } =>
                    kbd.set_key(&mut keys, false, kc),
                _ => ()
            };

            let mut write_lock =
                input_sender.write().unwrap();
            *write_lock = keys.clone();
        }

        if let Ok(array_ref) = array_receiver.read() {

            for (i, row) in (*array_ref).iter().enumerate() {
                for (j, &should_draw) in row.iter().enumerate() {
                    if should_draw {
                        canvas.set_draw_color(Color::RGB(255,255,255));
                    } else {
                        canvas.set_draw_color(Color::RGB(0,0,0));
                    }
                    canvas.fill_rect(Rect::new(i as i32 * 10,
                                               j as i32 * 10,
                                               10, 10)).unwrap();

                }
            }

            canvas.present();
        }
    }

    let mut stop_lock = stop.write().unwrap();
    *stop_lock = true;
}


fn main () -> () {

    /* Dealing with the command line */

    let matches =
        App::new(PROG_NAME)
        .version("0.1")
        .author("Guerric Chupin")
        .arg(Arg::with_name("ROM")
             .help("The ROM to load")
             .required(true)
        )
        .arg(Arg::with_name("print_opcode")
             .short("p")
             .long("print-opcode")
        )
        .arg(Arg::with_name("keyboard")
             .short("k")
             .long("keyboard")
             .takes_value(true)
             .possible_values(&["azerty", "qwerty"])
        )
        .get_matches();

    let kbd =
        match matches.value_of("keyboard") {
            None | Some("azerty") => SupportedKeyborads::AZERTY,
            Some("qwerty") => SupportedKeyborads::QWERTY,
            _ => panic!(),
        };

    /* Setting up the renderer */

    let array_lock = Arc::new(RwLock::new([[false; 32]; 64]));
    let input_lock = Arc::new(RwLock::new([false; 16]));

    let array_receiver = array_lock.clone();
    let input_sender = input_lock.clone();

    let array_send = array_lock.clone();
    let input_receiver = input_lock.clone();


    let stop = Arc::new(RwLock::new(false));
    let stop_gui = stop.clone();

    spawn(move || gui(kbd,
                      stop_gui.deref(),
                      array_receiver.deref(),
                      input_sender.deref()));


    /* Starting up the CPU */

    let debug_flags =
        DebugFlags {
            print_opcode: matches.is_present("print_opcode"),
            print_raw_code: false,
        };

    let config =
        Config {
            clock_gap: 2
        };


    let mut cpu = {
        let rom_file_name = matches.value_of("ROM").unwrap();

        let mut rom_file =
            File::open(rom_file_name)
            .expect(&format!("File not found {}", rom_file_name));

        let mut rom = Vec::with_capacity(4096);
        rom_file.read_to_end(&mut rom).unwrap();

        CPU::init(&rom, config, debug_flags)
    };

    loop {

        {
            let stop_lock = stop.read().unwrap();
            if *stop_lock {
                break;
            }
        }

        let keys = {
            input_receiver.deref().read().unwrap().clone()
        };

        match cpu.step(keys) {
            None => (),
            Some(array) => {
                let mut write_lock = array_send.write().unwrap();
                *write_lock = array;
            }
        };

        sleep(Duration::from_millis(cpu.get_clock_gap()));
    }

    let mut stop_lock = stop.write().unwrap();
    *stop_lock = true;
}
