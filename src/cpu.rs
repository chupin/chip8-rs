extern crate rand;

use std::time::{Instant};

type Addr = u16;

type RegId = u8;

#[derive(Debug, Clone, Copy)]
pub enum OpCode {
    CallProg(Addr),
    ClearScreen,
    Return,
    Goto(Addr),
    CallFun(Addr),
    EqConst(RegId, u8),       // Vx == c
    NEqConst(RegId, u8),      // Vx != c
    Eq(RegId, RegId),         // Vx == Vy
    SetConst(RegId, u8),      // Vx := c
    IncConst(RegId, u8),      // Vx += c
    Set(RegId, RegId),        // Vx := Vy
    OrWith(RegId, RegId),     // Vx |= Vy
    AndWith(RegId, RegId),    // Vx &= Vy
    XorWith(RegId, RegId),    // Vx ^= Vy
    IncBy(RegId, RegId),      // Vx += Vy
    DecBy(RegId, RegId),      // Vx -= Vy
    RShift(RegId, RegId),     // Vx := Vy // Vy := Vy >> 1
    BySub(RegId, RegId),      // Vx := Vy - Vx
    LShift(RegId, RegId),     // Vx := Vy // Vy := Vy << 1
    Jump1IfNeq(RegId, RegId), // if (Vx != Vy) jump 1 instruction
    SetI(Addr),               // I := N
    JumpPC(Addr),             // Pc := V0 + NNN
    SetRandAnd(RegId, u8),    // Vx := rand() & NN
    Draw(RegId, RegId, u8),   // Draw a sprite at Vx, Vy of width 8
    // pixels and of height N
    JumpIfPressed(RegId),
    JumpIfNotPressed(RegId),
    GetDelay(RegId),
    GetKey(RegId),
    SetDelayTimer(RegId),
    SetSoundTimer(RegId),
    IncI(RegId),
    SetSpriteAddr(RegId),
    SetBCD(RegId),
    RegDump(RegId),
    RegLoad(RegId),
}

pub struct DebugFlags {
    pub print_raw_code: bool,
    pub print_opcode: bool,
}

pub struct Config {
    pub clock_gap: u64
}

pub type GFX = [[bool; 32]; 64];

pub struct CPU {
    flags: DebugFlags,

    config: Config,

    last_frame_time: Instant,

    index_reg: u16,
    pc: u16,

    delay_timer: u8,
    sound_timer: u8,


    sp: u16,
    stack: [u16; 16],

    keys: [bool; 16],

    regs: [u8; 16],
    mem: [u8; 4096],

    gfx: GFX,
    draw: bool,
}

const FONTSET: [u8; 80] =
    [
        0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
        0x20, 0x60, 0x20, 0x20, 0x70, // 1
        0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
        0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
        0x90, 0x90, 0xF0, 0x10, 0x10, // 4
        0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
        0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
        0xF0, 0x10, 0x20, 0x40, 0x40, // 7
        0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
        0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
        0xF0, 0x90, 0xF0, 0x90, 0x90, // A
        0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
        0xF0, 0x80, 0x80, 0x80, 0xF0, // C
        0xE0, 0x90, 0x90, 0x90, 0xE0, // D
        0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
        0xF0, 0x80, 0xF0, 0x80, 0x80  // F
    ];

impl CPU {
    pub fn get_clock_gap(&self) -> u64 {
        self.config.clock_gap
    }

    pub fn init(rom: &[u8], config: Config, flags: DebugFlags) -> CPU {
        let mut mem = [0; 4096];

        for i in 0..80 {
            mem[i] = FONTSET[i]
        }

        for i in 0..rom.len() {
            mem[i + 512] = rom[i]
        }

        CPU {
            config: config,
            flags: flags,

            last_frame_time: Instant::now(),

            index_reg: 0,
            pc: 0x200,

            delay_timer: 0,
            sound_timer: 0,

            sp: 0,
            stack: [0; 16],

            keys: [false; 16],

            regs: [0; 16],

            mem: mem,

            draw: false,
            gfx: [[false; 32]; 64],
        }
    }

    fn fetch(&self) -> u16 {

        let i1 = self.mem[self.pc as usize] as u16;
        let i2 = self.mem[(self.pc + 1) as usize] as u16;

        let raw_code = i1 << 8 | i2;

        if self.flags.print_raw_code {
            println!("{:x}", raw_code);
        }

        raw_code
    }


    fn execute(&mut self, opcode: OpCode) -> () {
        use self::OpCode::*;

        match opcode {
            CallProg(_) => {
                println!("Ignored instruction {:?}", opcode);
                self.pc += 2;
            },
            ClearScreen => {
                for line in self.gfx.iter_mut() {
                    for row in line.iter_mut() {
                        *row = false;
                    }
                }
                self.pc += 2;
            },
            Return => {
                self.pc = self.stack[(self.sp as usize) - 1] + 2;
                self.sp -= 1;

            },
            Goto(addr) => {
                self.pc = addr;
            }
            CallFun(addr) => {
                self.stack[self.sp as usize] = self.pc;
                self.sp += 1;
                self.pc = addr;
            },
            EqConst(reg, val) => {
                if self.regs[reg as usize] == val {
                    self.pc += 2;
                }
                self.pc += 2;
            }
            NEqConst(reg, val) => {
                if self.regs[reg as usize] != val {
                    self.pc += 2;
                }
                self.pc += 2;
            },
            Eq(r1, r2) => {
                if self.regs[r1 as usize] == self.regs[r2 as usize] {
                    self.pc += 2;
                }
                self.pc += 2;
            },
            SetConst(reg, c) => {
                self.regs[reg as usize] = c;
                self.pc += 2;
            },
            IncConst(reg, c) => {
                self.regs[reg as usize] = self.regs[reg as usize]
                    .wrapping_add(c);
                self.pc += 2;
            },
            Set(r1, r2) => {
                self.regs[r1 as usize] = self.regs[r2 as usize];
                self.pc += 2;
            },
            OrWith(r1, r2) => {
                self.regs[r1 as usize] |= self.regs[r2 as usize];
                self.pc += 2;
            },
            AndWith(r1, r2) => {
                self.regs[r1 as usize] &= self.regs[r2 as usize];
                self.pc += 2;
            },
            XorWith(r1, r2) => {
                self.regs[r1 as usize] ^= self.regs[r2 as usize];
                self.pc += 2;
            },
            IncBy(r1, r2) => {
                let (res, ovf) = self.regs[r1 as usize]
                    .overflowing_add(self.regs[r2 as usize]);
                self.regs[r1 as usize] = res;
                self.regs[0xf] = ovf as u8;
                self.pc += 2;
            },
            DecBy(r1, r2) => {
                let (res, ovf) = self.regs[r1 as usize]
                    .overflowing_sub(self.regs[r2 as usize]);
                self.regs[r1 as usize] = res;
                self.regs[0xf] = !ovf as u8;
                self.pc += 2;
            },
            RShift(vx, vy) => {
                self.regs[0xf] = self.regs[vy as usize] % 2;
                self.regs[vy as usize] = self.regs[vy as usize] >> 1;
                self.regs[vx as usize] = self.regs[vy as usize];
                self.pc += 2;
            },
            BySub(r1, r2) => {
                let (r1_val, ovf) = self.regs[r2 as usize]
                    .overflowing_sub(self.regs[r1 as usize]);
                self.regs[r1 as usize] = r1_val;
                self.regs[0xf] = !ovf as u8;
                self.pc += 2;
            },
            LShift(r1, r2) => {
                self.regs[0xf] = self.regs[r2 as usize] >> 7;
                self.regs[r2 as usize] = self.regs[r2 as usize] << 1;
                self.regs[r1 as usize] = self.regs[r2 as usize];
                self.pc += 2;
            },
            Jump1IfNeq(r1, r2) => {
                if self.regs[r1 as usize] != self.regs[r2 as usize] {
                    self.pc += 2;
                }
                self.pc += 2;
            },
            SetI(addr) => {
                self.index_reg = addr;
                self.pc += 2;
            },
            JumpPC(addr) => {
                self.pc = addr + (self.regs[0] as u16);
            },
            SetRandAnd(reg, c) => {
                self.regs[reg as usize] = rand::random::<u8>() & c;
                self.pc += 2;
            },
            Draw(r1, r2, height) => {
                let x = self.regs[r1 as usize];
                let y = self.regs[r2 as usize];

                self.regs[0xf] = 0;

                for yl in 0..height {
                    let pixel = self.mem[(self.index_reg + (yl as u16)) as usize];

                    for xl in 0..8 {
                        if pixel & (0x80 >> xl) != 0 {
                            let x_ind = (x + xl) as usize;
                            let y_ind = (y + yl) as usize;
                            if x_ind < 64 && y_ind < 32 {
                                if self.gfx[(x + xl) as usize][(y + yl) as usize] {
                                    self.regs[0xf] = 1;
                                }
                                self.gfx[(x + xl) as usize][(y + yl) as usize] ^= true;
                            }
                        }
                    }
                }

                self.draw = true;
                self.pc += 2;
            },
            JumpIfPressed(reg) => {
                if self.keys[self.regs[reg as usize] as usize] {
                    self.pc += 2;
                }
                self.pc += 2;
            },
            JumpIfNotPressed(reg) => {
                if !self.keys[self.regs[reg as usize] as usize] {
                    self.pc += 2;
                }
                self.pc += 2;
            },
            GetDelay(reg) => {
                self.regs[reg as usize] = self.delay_timer;
                self.pc += 2;
            },
            GetKey(reg) => {
                for (i, &k) in self.keys.iter().enumerate() {
                    if k {
                        self.regs[reg as usize] = i as u8;
                        self.pc += 2;
                        break;
                    }
                }
            },
            SetDelayTimer(reg) => {
                self.delay_timer = self.regs[reg as usize];
                self.pc += 2;
            },
            SetSoundTimer(reg) => {
                self.sound_timer = self.regs[reg as usize];
                self.pc += 2;
            },
            IncI(reg) => {
                self.index_reg += self.regs[reg as usize] as u16;
                self.pc += 2;
            },
            SetSpriteAddr(reg) => {
                // Lookup the sprite for the given key Sprites are
                // stored from memory address 0 and each sprites takes
                // up 5 bytes
                self.index_reg = (self.regs[reg as usize] * 5) as u16;
                self.pc += 2;
            },
            SetBCD(reg) => {
                let val = self.regs[reg as usize];
                self.mem[self.index_reg as usize] = (val / 100) % 10;
                self.mem[(self.index_reg + 1) as usize] = (val / 10) % 10;
                self.mem[(self.index_reg + 2) as usize] = val % 10;
                self.pc += 2;
            },
            RegDump(reg) => {
                for i in 0..(reg + 1) {
                    self.mem[self.index_reg as usize] = self.regs[i as usize];
                    self.index_reg += 1;
                }

                self.pc += 2;
            },
            RegLoad(reg) => {
                for i in 0..(reg + 1) {
                    self.regs[i as usize] = self.mem[self.index_reg as usize];
                    self.index_reg += 1;
                }

                self.pc += 2;
            },
        }
    }

    fn handle_timers(&mut self) -> () {
        let dur = self.last_frame_time.elapsed();

        if dur.as_secs() != 0 || dur.subsec_nanos() >= 16_000_000 {
            if self.delay_timer != 0 {
                self.delay_timer -= 1;
            }
            if self.sound_timer != 0 {
                self.sound_timer -= 1;
            }
            self.last_frame_time = Instant::now();
        }
    }


    pub fn step(&mut self, keys: [bool; 16]) -> Option<GFX> {
        self.keys = keys;
        let opcode = decode(self.fetch());
        if self.flags.print_opcode {
            println!("Opcode: {:?}", opcode);
        }
        self.execute(opcode);
        self.handle_timers();

        if self.draw {
            Some(self.gfx.clone())
        } else {
            None
        }
    }


}

pub fn decode(raw_code: u16) -> OpCode {
    use self::OpCode::*;

    /* Utility functions for decoding opcodes */

    fn get_first(x: u16) -> u8 {
        (x >> 12) as u8
    }

    fn get_sec(x: u16) -> u8 {
        ((x & 0x0f00) >> 8) as u8
    }

    fn get_third(x: u16) -> u8 {
        ((x & 0x00f0) >> 4) as u8
    }

    fn get_fourth(x: u16) -> u8 {
        (x & 0x000f) as u8
    }

    fn get_addr(x: u16) -> u16 {
        x & 0x0fff
    }

    fn get_const(x: u16) -> u8 {
        (x & 0x00ff) as u8
    }

    fn decode_panic(raw_code: u16) -> ! {
        panic!("Cannot decode {:x}.", raw_code)
    }

    let code_id = get_first(raw_code);

    match code_id {
        _ if raw_code == 0x00e0  =>
            ClearScreen,
        _ if raw_code == 0x00ee =>
            Return,
        0 =>
            CallProg(get_addr(raw_code)),
        1 =>
            Goto(get_addr(raw_code)),
        2 =>
            CallFun(get_addr(raw_code)),
        3 =>
            EqConst(get_sec(raw_code), get_const(raw_code)),
        4 =>
            NEqConst(get_sec(raw_code), get_const(raw_code)),
        5 =>
            Eq(get_sec(raw_code), get_third(raw_code)),
        6 =>
            SetConst(get_sec(raw_code), get_const(raw_code)),
        7 =>
            IncConst(get_sec(raw_code), get_const(raw_code)),
        8 => {
            let vx = get_sec(raw_code);
            let vy = get_third(raw_code);
            match get_fourth(raw_code) {
                0 => Set(vx, vy),
                1 => OrWith(vx, vy),
                2 => AndWith(vx, vy),
                3 => XorWith(vx, vy),
                4 => IncBy(vx, vy),
                5 => DecBy(vx, vy),
                6 => RShift(vx, vy),
                7 => BySub(vx, vy),
                0xe => LShift(vx, vy),
                _ => decode_panic(raw_code)
            }
        },
        9 =>
            match get_fourth(raw_code) {
                0 =>
                    Jump1IfNeq(get_sec(raw_code), get_third(raw_code)),
                _ =>
                    decode_panic(raw_code),
            },
        0xa =>
            SetI(get_addr(raw_code)),
        0xb =>
            JumpPC(get_addr(raw_code)),
        0xc =>
            SetRandAnd(get_sec(raw_code), get_const(raw_code)),
        0xd =>
            Draw(get_sec(raw_code), get_third(raw_code), get_fourth(raw_code)),
        0xe =>
            match get_const(raw_code) {
                0x9e =>
                    JumpIfPressed(get_sec(raw_code)),
                0xa1 =>
                    JumpIfNotPressed(get_sec(raw_code)),
                _ =>
                    decode_panic(raw_code),
            },
        0xf => {
            let vx = get_sec(raw_code);
            match get_const(raw_code) {
                0x07 =>
                    GetDelay(vx),
                0x0a =>
                    GetKey(vx),
                0x15 =>
                    SetDelayTimer(vx),
                0x18 =>
                    SetSoundTimer(vx),
                0x1e =>
                    IncI(vx),
                0x29 =>
                    SetSpriteAddr(vx),
                0x33 =>
                    SetBCD(vx),
                0x55 =>
                    RegDump(vx),
                0x65 =>
                    RegLoad(vx),
                _ =>
                    decode_panic(raw_code),
            }
        },
        _ =>
            decode_panic(raw_code)
    }
}
